package neo4j;

import neo4j.nodes.*;
import neo4j.repositories.*;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.util.Optionals;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.Set;

@RestController
@EnableNeo4jRepositories
public class RestApiController {

    private AuthorRepository authorRepository;
    private CommentRepository commentRepository;
    private PostRepository postRepository;
    private SectionRepository sectionRepository;
    private TopicRepository topicRepository;
    private Driver driver;

    private String uri = "bolt://localhost:7687";
    private String user = "neo";
    private String password = "neo";

    public RestApiController(AuthorRepository authorRepository,
                             CommentRepository commentRepository,
                             PostRepository postRepository,
                             SectionRepository sectionRepository,
                             TopicRepository topicRepository) {
        this.authorRepository = authorRepository;
        this.commentRepository = commentRepository;
        this.postRepository = postRepository;
        this.sectionRepository = sectionRepository;
        this.topicRepository = topicRepository;
        this.driver = GraphDatabase.driver(uri, AuthTokens.basic(user, password));
    }

    @RequestMapping("/author")
    public Author authorQuery(
            @RequestParam(value = "name") String name,
            @RequestParam(value = "topic", defaultValue = ".*") String topic,
            @RequestParam(value = "section", defaultValue = ".*") String section)
    {
        boolean sectionFilter = !section.equals(".*");
        boolean topicFilter = !topic.equals(".*");

        if(topicFilter && !sectionFilter) {
            return authorRepository.findByNameTopic(name, topic);
        } else if(!topicFilter && sectionFilter) {
            return authorRepository.findByNameSection(name, section);
        } else if(topicFilter && sectionFilter) {
            return authorRepository.findByNameTopicSection(name, topic, section);
        } else {
            return authorRepository.findByName(name);
        }
    }

    @RequestMapping("/author/{id}")
    public Author authorById(@PathVariable Long id) {
        return (Author)Optionals.toStream(authorRepository.findById(id)).toArray()[0];
    }

    @RequestMapping("/comment")
    public Set<Comment> commentQuery(@RequestParam(value = "author", defaultValue = "") String author,
                                     @RequestParam(value = "postId", defaultValue = "") String postId,
                                     @RequestParam(value = "childOf", defaultValue = "") String parentId,
                                     @RequestParam(value = "postTopic", defaultValue = "") String postTopic,
                                     @RequestParam(value = "postSection", defaultValue = "") String postSection) {

        if(!author.isEmpty()) return commentRepository.findByAuthor(author);
        if(!postId.isEmpty()) return commentRepository.findByPostId(postId);
        if(!parentId.isEmpty()) return commentRepository.findByParentId(parentId);
        if(!postTopic.isEmpty()) return commentRepository.findByPostTopic(postId);
        if(!postSection.isEmpty()) return commentRepository.findByPostSection(postId);
        return new HashSet<>();
    }

    @RequestMapping("/post")
    public Set<Post> posts(@RequestParam(value = "author", defaultValue = "") String author,
                           @RequestParam(value = "topic", defaultValue = "") String topic,
                           @RequestParam(value = "section", defaultValue = "") String section) {
        if(!author.isEmpty()) return postRepository.findByAuthor(author);
        if(!topic.isEmpty()) return postRepository.findByTopic(topic);
        if(!section.isEmpty()) return postRepository.findBySection(section);

        return new HashSet<>();
    }

    @RequestMapping("/section")
    public Set<Section> sections(@RequestParam(value = "name") String name) {
        return sectionRepository.findByName(name);
    }

    @RequestMapping("/topic")
    public Set<Topic> topics(@RequestParam(value = "name") String name) {
        return topicRepository.findByName(name);
    }

}
