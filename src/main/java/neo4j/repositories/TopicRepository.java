package neo4j.repositories;

import neo4j.nodes.Topic;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface TopicRepository extends CrudRepository<Topic, Long> {
    Set<Topic> findByName(@Param(value = "name") String name);
}
