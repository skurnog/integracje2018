package neo4j.repositories;

import neo4j.nodes.Content;
import neo4j.nodes.Post;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface PostRepository extends CrudRepository<Post, Long> {
    Set<Post> findByAuthor(@Param(value = "author") String author);

    @Query("match p=(n:Post)-[:belongToTopic]->(t:Topic) where t.name={topic} return p")
    Set<Post> findByTopic(@Param(value = "topic") String topic);

    @Query("match p=(n:Post)-[:belongToSection]->(s:Section) where s.name={section} return p")
    Set<Post> findBySection(@Param(value = "section") String section);
}
