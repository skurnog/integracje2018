package neo4j.repositories;

import neo4j.nodes.Section;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface SectionRepository extends CrudRepository<Section, Long> {
    Set<Section> findByName(@Param(value = "name") String name);
}
