package neo4j.repositories;

import neo4j.nodes.Author;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestParam;

public interface AuthorRepository extends CrudRepository<Author, Long> {
    Author findByName(String name);

    @Query("MATCH p=(n:Author)-[:Posted]->()-[:belongToTopic]->(topic:Topic), p2=(n:Author)-[:Commented]-() " +
            "where n.name = {name} and topic.name =~ ({topic}) RETURN p,p2")
    Author findByNameTopic(@Param(value = "name") String name,
                           @Param(value = "topic") String topic);

    @Query("MATCH p=(n:Author)-[:Posted]->()-[:belongToSection]->(section:Section), p2=(n:Author)-[:Commented]-() " +
            "where n.name = {name} and section.name =~ ({section}) RETURN p,p2")
    Author findByNameSection(@Param(value = "name") String name,
                             @Param(value = "section") String section);

    @Query("MATCH " +
            "p=(n:Author)-[:Posted]->()-[:belongToSection]->(section:Section), " +
            "p2=(n:Author)-[:Commented]-(), " +
            "p3=(n:Author)-[:Posted]->()-[:belongToTopic]->(topic:Topic)" +
            "where n.name = {name} and topic.name =~ ({topic} and section.name =~ ({section}) " +
            "RETURN p,p2,p3")
    Author findByNameTopicSection(@Param(value = "name") String name,
                                  @Param(value = "topic") String topic,
                                  @Param(value = "section") String section);
}
