package neo4j.repositories;

import neo4j.nodes.Comment;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface CommentRepository extends CrudRepository<Comment, Long> {
    Set<Comment> findByAuthor(@Param(value = "author") String author);

    @Query("MATCH (n:Comment)-[:On]->(post:Post) WHERE post.id = {postId} RETURN n")
    Set<Comment> findByPostId(@Param("postId") String postId);

    @Query("MATCH (n:Comment)-[:childOf]->() WHERE n.parentId = {parentId} RETURN n")
    Set<Comment> findByParentId(@Param("parentId") String parentId);

    @Query("MATCH (n:Comment)-[:On]->()->[:belongToTopic]->(topic:Topic) WHERE topic.name = {topicId} RETURN n")
    Set<Comment> findByPostTopic(@Param("topicId") String topicId);

    @Query("MATCH (n:Comment)-[:On]->()->[:belongToSection]->(section:Section) WHERE section.name = {sectionId} RETURN n")
    Set<Comment> findByPostSection(@Param("sectionId") String sectionId);
}
