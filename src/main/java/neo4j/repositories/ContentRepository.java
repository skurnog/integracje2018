package neo4j.repositories;

import neo4j.nodes.Content;
import org.springframework.data.repository.CrudRepository;

public interface ContentRepository extends CrudRepository<Content, Long> {
//    Content getContent(Long id);
}
