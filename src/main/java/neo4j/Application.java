package neo4j;

import neo4j.nodes.Author;
import neo4j.repositories.AuthorRepository;
import neo4j.repositories.CommentRepository;
import org.neo4j.driver.internal.util.Iterables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

import java.util.*;

@SpringBootApplication
@EnableNeo4jRepositories
public class Application {

	private final static Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) throws Exception {
		SpringApplication.run(Application.class, args);
	}

//	@Bean
//	CommandLineRunner demo(AuthorRepository authorRepository, CommentRepository commentRepository) {
//        return args -> {
//
//            List<Author> allAuthors = Iterables.asList(authorRepository.findAll());
//            for(Author author: allAuthors) {
//                System.out.println(author.toJSON());
//            }
//        };
//	}
}
