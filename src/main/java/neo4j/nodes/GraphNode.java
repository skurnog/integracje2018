package neo4j.nodes;

import com.google.gson.Gson;

public class GraphNode {
    public String toJSON() {
        return new Gson().toJson(this);
    }
}
