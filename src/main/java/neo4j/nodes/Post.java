package neo4j.nodes;

import neo4j.nodes.Content;
import neo4j.nodes.GraphNode;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Set;

@NodeEntity
public class Post extends GraphNode {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private Long date;
    private Long time;
    private Long views;

    @Relationship(type = "postContains", direction = Relationship.UNDIRECTED)
    private Set<Content> content;

    @Relationship(type = "belongToSection", direction = Relationship.UNDIRECTED)
    private Set<Content> section;

    public Post() {
    }

    public Post(Long id, String name, Long date, Long time, Long views) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.time = time;
        this.views = views;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Long getViews() {
        return views;
    }

    public void setViews(Long views) {
        this.views = views;
    }

    public Set<Content> getContent() {
        return content;
    }

    public void setContent(Set<Content> content) {
        this.content = content;
    }

    public Set<Content> getSection() {
        return section;
    }

    public void setSection(Set<Content> section) {
        this.section = section;
    }
}
