package neo4j.nodes;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Set;

@NodeEntity
public class Comment extends GraphNode {
    @Id
    @GeneratedValue
    private Long id;
    private String author;
    private Long date;
    private Long likes;
    private Long dislikes;
    private Long parentCommentId;
    private Long time;

    @Relationship(type = "commentContains", direction = Relationship.UNDIRECTED)
    public Set<Content> content;

    @Relationship(type = "childOf", direction = Relationship.UNDIRECTED)
    public Set<Comment> children;

    @Relationship(type = "On", direction = Relationship.UNDIRECTED)
    public Set<Post> parentPost;

    public Comment() {
    }

    public Comment(Long id, String author, Long date, Long likes, Long dislikes, Long parentCommentId, Long time) {
        this.id = id;
        this.author = author;
        this.date = date;
        this.likes = likes;
        this.dislikes = dislikes;
        this.parentCommentId = parentCommentId;
        this.time = time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Long getLikes() {
        return likes;
    }

    public void setLikes(Long likes) {
        this.likes = likes;
    }

    public Long getDislikes() {
        return dislikes;
    }

    public void setDislikes(Long dislikes) {
        this.dislikes = dislikes;
    }

    public Long getParentCommentId() {
        return parentCommentId;
    }

    public void setParentCommentId(Long parentCommentId) {
        this.parentCommentId = parentCommentId;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Set<Content> getContent() {
        return content;
    }

    public void setContent(Set<Content> contentSet) {
        this.content = contentSet;
    }

    public Set<Comment> getChildren() {
        return children;
    }

    public void setChildren(Set<Comment> children) {
        this.children = children;
    }

    public Set<Post> getParentPost() {
        return parentPost;
    }

    public void setParentPost(Set<Post> parentPost) {
        this.parentPost = parentPost;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}

