package neo4j.nodes;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Set;

@NodeEntity
public class Author extends GraphNode {

	@Id @GeneratedValue private Long id;
	private String name;
	private String bloglink;

	private Author() {
	}

	public Author(String name, String bloglink) {
		this.name = name;
		this.bloglink = bloglink;
	}

	@Relationship(type = "Posted", direction = Relationship.UNDIRECTED)
	public Set<Post> posts;

	@Relationship(type = "Commented", direction = Relationship.UNDIRECTED)
	public Set<Comment> comments;

	public String toString() {
		return this.name + " -> " + this.bloglink;
	}

	public String getName() {
		return name;
	}
	public String getBloglink() {
		return bloglink;
	}
	public Set<Post> getPosts() { return posts; }
	public Set<Comment> getComments() { return comments; }

	public void setName(String name) {
		this.name = name;
	}
	public void setBloglink(String bloglink) {
		this.bloglink = bloglink;
	}
	public void setPosts(Set<Post> posts) { this.posts = posts; }
	public void setComments(Set<Comment> comments) { this.comments = comments; }
}
